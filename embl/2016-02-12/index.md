---

layout: bright
line_numbers: false
default_body_class: full

style: |

    #Cover h2 {
        margin:30px 0 0;
        color:#000;
        text-align:center;
        font-size:70px;
        }
    #Cover p {
        margin:10px 0 0;
        text-align:center;
        color:#FFF;
        font-style:italic;
        font-size:20px;
        }
        #Cover p a {
            color:#FFF;
            }
    #Picture h2 {
        color:#FFF;
        }
    #SeeMore h2 {
        font-size:100px
        }
    #SeeMore img {
        width:0.72em;
        height:0.72em;
        }
---

# SM_standalone:<br/>status update {#Cover}
{:.slide}

## Interactivity problem

- SM engine output is static
- ...what to do when questions arise?
- ...earlier I used to fire up SCiLS Lab
    - not designed for looking at _molecules_
    - doesn't give a lot of insight
- ...now we have a solution!

## webserver

{:.cover #webserver}

<img style="margin-top: -200px;width: 95%;" src="pictures/webserver.png" alt="webserver" />
    
## New webserver features

- Any formula
- Any dataset
- Any (reasonable) ppm & resolution values
- Diagnostic plots
  - isotope peak intensities correlations
  - per-pixel isotope scores

## A bit of history

- initially one dataset at a time was supported
- fully loaded into memory
- problems:
  - large datasets couldn't be loaded
  - slow start

## Friendlier data format

- sorted records (m/z, intensity, x, y)
- when indexed, allows fast retrieval of images
  - at the expense of slow access to spectra
- prototype: imzML → *pyTables*
  - worked well for the webserver
  - but conversion was horribly slow :(

## Home-grown format: .imzb

- same records (m/z, intensity, x, y)
- packed into compressed blocks
  - each block holds at most 4k records
  - light & fast compression: *blosc*
- index and metadata in a separate file
  - first m/z and file offset for each block
  - image dimensions
  
## Implementation

- language: C++11
- reading files from Python: through **CFFI**
  - calls shared library functions
  - the overhead is minimal
  
- resulting files are smaller than .imzML/.ibd
- conversion time: 1m 40s for a 5.6 GB file

## Porting other things to C++

- sum formula parser ✔
- isotope pattern calculation ✔
- isotope pattern score ✔
- image correlation score ✔
- measure of chaos ✔

~2k lines of code in total

## Distinguishing features

- ...No unit tests!
- ...No documentation!
- ...No users!

## Distinguishing features

- Multi-threading (OpenMP)
- Low memory consumption
- Unix way
- Performance
  - Example: 5 GB .imzML/.ibd input, 10k molecules, 3 adducts
    - conversion to .imzb: 100s using 1 thread
    - computing metrics: 45s using 4 threads

## Usage

    $ ./isocalc --resolution=140000 --adducts=H,K,Na
      sum_formulas.txt hmdb_140k.db
    $ ./convert zebrafish.imzML zebrafish.imzb
    $ ./detect --ppm=3 hmdb_140k.db zebrafish.imzb
      --out=metrics.txt

## Usage

    $ LC_ALL=C sort metrics.txt -grk3 | head -n5

| C19H24N2O2	|+K	|0.994346	|0.996325	|0.99831|
| C19H24N2O2	|+Na|0.985496   |0.997023   |0.998613|
| C12H13N2O	    |+H	|0.983776	|0.998738	|0.99827|
| C11H12N2	    |+H	|0.964385	|0.997585	|0.998369|
| C11H20O16P2	|+H	|0.957928	|0.973691	|0.999536|

## Plans for the near future

- calculating FDR
- finding new metrics giving lower FDR
  - with sane turnaround time!
- exposing more functionality to Python
